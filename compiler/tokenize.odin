package compiler

import "core:fmt"


Token :: struct {
    lexeme: string,
    type: TokenType,
    line: int,
}


Tokenizer :: struct {
    source: string,
    start: int,
    pointer: int,
    line: int,
}

init_tokenizer :: proc(source: string) {
    tokenizer.source = source 
    tokenizer.pointer = 0
    tokenizer.line = 0 
}


tokenizer : Tokenizer = Tokenizer {}

tokenize :: proc(source: string) -> []Token {
    init_tokenizer(source)
    current_line := -1
    for {
        token := scan_token()

        if token.line != current_line {
            fmt.printf("\nLINE: %4v ", token.line)
        } else {
            fmt.print(" | ")
        }
        tokenizer.line = token.line
        if token.type == .EOF {
            fmt.print("EOF")
            break
        } else if token.type == .ERROR {
            fmt.printf("ERROR: '%v'", token.lexeme)
            return make([]Token, 1)
        } else {
            fmt.printf("'%v' '%v'", token.lexeme, token.type)
        }
    }
    fmt.println()

    tokens := make([]Token, 1)
    return tokens
}



TokenPair :: struct {
    target: u8,
    type: TokenType
}

single_chars :: [?]TokenPair {
    TokenPair { '(', .LEFT_PAREN },
    TokenPair { ')', .RIGHT_PAREN },
    TokenPair { '{', .LEFT_BRACE },
    TokenPair { '}', .RIGHT_BRACE },
    TokenPair { ';', .SEMICOLON },
    TokenPair { ',', .COMMA },
    TokenPair { '.', .DOT },
    TokenPair { '-', .MINUS },
    TokenPair { '+', .PLUS },
    TokenPair { '/', .SLASH },
    TokenPair { '*', .STAR }
    TokenPair { '!', .BANG }
}

CompoundToken :: struct {
    initial: u8,
    next: u8,
    itype: TokenType,
    ntype: TokenType,
}

compound_tokens :: [?]CompoundToken {
    CompoundToken { '!', '=', .BANG, .BANG_EQUAL },
    CompoundToken { '=', '=', .EQUAL, .EQUAL_EQUAL },
    CompoundToken { '>', '=', .GREATER, .GREATER_EQUAL },
    CompoundToken { '<', '=', .LESS, .LESS_EQUAL },
}

advance_tokenizer :: proc() {
    tokenizer.pointer += 1
}

peek :: proc() -> u8 {
    return tokenizer.source[tokenizer.pointer]
}

peek_next :: proc() -> u8 {
    return tokenizer.source[tokenizer.pointer + 1]
}

compound_match :: proc(current_char: u8, initial: u8, next: u8, itype: TokenType, ntype: TokenType) -> (Token, bool) {
    if current_char == initial {
        advance_tokenizer()
        if is_at_end() {
            return make_token(itype), true
        }

        next_char := peek()
        if next_char == next {
            advance_tokenizer()
            if is_at_end() || peek() == ' ' {
                return make_token(ntype), true
            } else {
                advance_tokenizer()
                return make_token(.IGNORE), true
            }
        } else if next_char == ' ' {
            return make_token(itype), true
        } else {
            advance_tokenizer()
            return make_token(.IGNORE), true
        }
    }

    return make_token(.IGNORE), false
}

skip_whitespace :: proc() {
    if is_at_end() {
        return 
    }
    // Handle whitespace
    current_char := peek()
    for current_char == ' ' || current_char == '\t' || current_char == '\n' {
        if current_char == '\n' {
            tokenizer.line += 1
        }

        advance_tokenizer()
        if is_at_end() {
            return 
        }
        current_char = peek()
    }
}


scan_token :: proc() -> Token {
    skip_whitespace()
    if is_at_end() {
        return make_token(.EOF)
    }

    current_char := peek()

    // Handle comments
    if current_char == '/' {
        if tokenizer.pointer < len(tokenizer.source) - 1 {
            next_char := peek_next() 
            if next_char == '/' {
                advance_tokenizer() // Consume first slash
                advance_tokenizer() // Consume second slash

                current_char = peek()
                for current_char != '\n' {
                    advance_tokenizer()
                    if is_at_end() {
                        return make_token(.EOF)
                    }

                    current_char = peek()
                }

                // Consume newline
                advance_tokenizer()
                if is_at_end() {
                    return make_token(.EOF)
                }

                skip_whitespace()
                if is_at_end() {
                    return make_token(.EOF)
                }
            }
        }
    }

    current_char = peek()


    tokenizer.start = tokenizer.pointer

    // Single character tokens
    for it in single_chars {
        target := it.target
        type   := it.type
        if current_char == target {
            advance_tokenizer()
            result := make_token(type)
            return result
        }
    }


    // One or two character tokens
    for it in compound_tokens {
        initial := it.initial
        next    := it.next
        itype   := it.itype
        ntype   := it.ntype


        token, ok := compound_match(current_char, initial, next, itype, ntype)
        if ok {
            return token
        }
    }

    // Number literal
    if is_numeric(current_char) {
        advance_tokenizer()
        if is_at_end() {
            return make_token(.NUMBER)
        }

        current_char = peek() 
        for is_numeric(current_char) {
            advance_tokenizer()
            if is_at_end() {
                return make_token(.NUMBER)
            }

            current_char = peek() 
        }

        if current_char == '.' {
            advance_tokenizer()
            if is_at_end() {
                return make_token(.NUMBER)
            }

            current_char = peek() 
            for is_numeric(current_char) {
                advance_tokenizer()
                if is_at_end() {
                    return make_token(.NUMBER)
                }

                current_char = peek() 
            }
        }

        // Dont check what the next token is
        // Let the parser handle any errors e.g. 2(
        return make_token(.NUMBER)
    }

    // String literal
    if current_char == '"' {
        advance_tokenizer()
        if is_at_end() {
            return make_token(.ERROR) // TODO: Make a good error message
        }

        current_char = peek()
        for current_char != '"' {
            advance_tokenizer()
            if is_at_end() {
                return make_token(.ERROR) // TODO: Make a good error message
            }

            current_char = peek()
            if current_char == '\n' {
                tokenizer.line += 1
            }
        }

        advance_tokenizer()
        return make_token(.STRING)
    }


    // Identifier or keyword
    if is_alpha(current_char) {
        advance_tokenizer()
        if is_at_end() {
            if is_keyword() {
                return make_keyword()
            } else {
                return make_token(.IDENTIFIER) 
            }
        }

        current_char = peek()
        for is_ident_char(current_char) {
            advance_tokenizer()
            if is_at_end() {
                if is_keyword() {
                    return make_keyword()
                } else {
                    return make_token(.IDENTIFIER) 
                }
            }

            current_char = peek()
        }
        
        if is_keyword() {
            return make_keyword()
        } else {
            return make_token(.IDENTIFIER) 
        }
    }

    // Handle all not .EOF tokens here
    advance_tokenizer()
    return make_token(.ERROR)
}

is_numeric :: proc(ch: u8) -> bool {
    return ch >= cast(u8) '0' && ch <= cast(u8) '9'
}

is_alpha :: proc(ch: u8) -> bool {
    is_lower_case := ch >= cast(u8) 'a' && ch <= cast(u8) 'z'
    is_upper_case := ch >= cast(u8) 'A' && ch <= cast(u8) 'Z'

    return is_lower_case || is_upper_case
}

is_ident_char :: proc(ch: u8) -> bool {
   is_alpha_p := is_alpha(ch)
   is_numeric_p := is_numeric(ch)
   is_underscore := ch == cast(u8) '_'

   return is_alpha_p || is_numeric_p || is_underscore
}

ctoi :: proc(ch: u8) -> int {
    return cast(int) (ch - cast(u8) '0')
}

make_token :: proc(type: TokenType) -> Token {
    lexeme := tokenizer.source[tokenizer.start:tokenizer.pointer]
    type := type
    line := tokenizer.line 

    return Token { lexeme, type, line }
}

KWP :: struct {
    lexeme: string,
    tokentype: TokenType,
}

keywords :: [?]KWP { 
    KWP { "class", .CLASS },
    KWP { "and", .AND },
    KWP { "else", .ELSE },
    KWP { "false", .FALSE },
    KWP { "for", .FOR },
    KWP { "fun", .FUN },
    KWP { "if", .IF },
    KWP { "nil", .NIL },
    KWP { "or", .OR },
    KWP { "print", .PRINT },
    KWP { "return", .RETURN },
    KWP { "super", .SUPER },
    KWP { "this", .THIS },
    KWP { "true", .TRUE },
    KWP { "var", .VAR },
    KWP { "while", .WHILE },
}

is_keyword :: proc() -> bool {
    lexeme := tokenizer.source[tokenizer.start:tokenizer.pointer]

    for kwp in keywords {
        if kwp.lexeme == lexeme {
            return true
        }
    }

    return false
}

make_keyword :: proc() -> Token {
    lexeme := tokenizer.source[tokenizer.start:tokenizer.pointer]
    for kwp in keywords {
        if kwp.lexeme == lexeme {
            return Token { lexeme, kwp.tokentype, tokenizer.line }
        }
    }

    return Token { lexeme, .ERROR, tokenizer.line }
}

is_at_end :: proc() -> bool {
    return tokenizer.pointer == len(tokenizer.source)
}
