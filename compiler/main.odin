package compiler

import "../vm"
import "core:fmt"
import "core:os"
import "core:strconv"

DEBUG_PRINT_CODE :: true

Parser :: struct {
    previous: Token,
    current : Token,
    had_error: bool,
    panic_mode: bool,
}

ParseFn :: proc(bool)

ParseRule :: struct {
    prefix: ParseFn,
    infix: ParseFn,
    precedence: Precedence,
}

MAX_LOCALS :: 256
Compiler :: struct {
    locals: [MAX_LOCALS]Local,
    local_count: int,
    scope_depth: int,
}


Local :: struct {
    name: Token,
    depth: int,
}

parser := Parser {}
current : ^Compiler = nil
compiling_chunk : ^vm.Chunk = nil


init_compiler :: proc(compiler: ^Compiler) {
    compiler.local_count = 0
    compiler.scope_depth = 0
    current = compiler
}

advance :: proc() {
    parser.previous = parser.current

    for {
        parser.current = scan_token()

        if (parser.current.type != .ERROR) {
            break
        }

        error_at_current(parser.current.lexeme)
    }
}


error_at_current :: proc(msg: string) {
    error_at(&parser.current, msg)
}

error :: proc(msg: string) {
    error_at(&parser.previous, msg)
}

error_at :: proc(token: ^Token, msg: string) {
    if parser.panic_mode {
        return
    }
    parser.panic_mode = true

    fmt.printf("Line %v: Error\n", token.line)

    if (token.type == .EOF) {
        fmt.println(" at end of file")
    } else if (token.type == .ERROR) {

    } else {
        fmt.printf(" at '%v'\n", token.lexeme)
    }

    fmt.printf("Message: %v\n", msg)
    parser.had_error = true
}


consume :: proc(type: TokenType, msg: string) {
    if parser.current.type != type {
        error_at_current(msg)
    } else {
        advance()
    }
}

emit_byte :: proc(byte: u8) {
    vm.write_chunk(current_chunk(), byte, parser.previous.line)
}

emit_bytes :: proc(byte1: u8, byte2: u8) {
    emit_byte(byte1)
    emit_byte(byte2)
}

emit_bytes_ops :: proc(byte1: vm.OpCode, byte2: vm.OpCode) {
    emit_byte(cast(u8) byte1)
    emit_byte(cast(u8) byte2)
}

emit_return :: proc() {
    emit_byte(cast(u8) vm.OpCode.OP_RETURN)
}

current_chunk :: proc() -> ^vm.Chunk {
    return compiling_chunk 
}


end_compiler :: proc() {
    emit_return()
    when DEBUG_PRINT_CODE {
        if !parser.had_error {
            fmt.println(vm.disassemble(current_chunk()))
        }
    }
}

binary :: proc(can_assign: bool) {
    operator_type := parser.previous.type
    rule := get_rule(operator_type) // Only to look up the current precedence
    parse_precedence(cast(Precedence) (cast(u8)rule.precedence + 1))

    #partial switch (operator_type) {
        case .PLUS:          emit_byte(cast(u8) vm.OpCode.OP_ADD); break;
        case .MINUS:         emit_byte(cast(u8) vm.OpCode.OP_SUB); break;
        case .STAR:          emit_byte(cast(u8) vm.OpCode.OP_MUL); break;
        case .SLASH:         emit_byte(cast(u8) vm.OpCode.OP_DIV); break;
        case .EQUAL_EQUAL:   emit_byte(cast(u8) vm.OpCode.OP_EQUAL); break;
        case .BANG_EQUAL:    emit_bytes_ops(vm.OpCode.OP_EQUAL, vm.OpCode.OP_NOT); break;
        case .GREATER:       emit_byte(cast(u8) vm.OpCode.OP_GREATER); break;
        case .GREATER_EQUAL: emit_bytes_ops(vm.OpCode.OP_LESS, vm.OpCode.OP_NOT); break;
        case .LESS:          emit_byte(cast(u8) vm.OpCode.OP_LESS); break;
        case .LESS_EQUAL:    emit_bytes_ops(vm.OpCode.OP_GREATER, vm.OpCode.OP_NOT); break;

        case: return; // Unreachable.
    }

}

get_rule :: proc(op_type: TokenType) -> ParseRule {
    return rules[op_type]
}



grouping :: proc(can_assign: bool) {
    expression()
    consume(.RIGHT_PAREN, "Expected ')' after expression.")
}

number :: proc(can_assign: bool) {
    value, _ := strconv.parse_f64(parser.previous.lexeme)
    emit_constant(value)
}

unary :: proc(can_assign: bool) {
    operator_type := parser.previous.type

    // Compile the operand.
    parse_precedence(.UNARY)

    // Emit the operator instruction.
    #partial switch operator_type {
        case .MINUS: emit_byte(cast(u8) vm.OpCode.OP_NEGATE)
        case .BANG: emit_byte(cast(u8) vm.OpCode.OP_NOT)
    }
}

literal :: proc(can_assign: bool) {
    #partial switch (parser.previous.type) {
        case .FALSE: emit_byte(cast(u8) vm.OpCode.OP_FALSE);
        case .NIL: emit_byte(cast(u8) vm.OpCode.OP_NIL);
        case .TRUE: emit_byte(cast(u8) vm.OpCode.OP_TRUE);
    }
}

compile_string :: proc(can_assign: bool) {
    string_token := parser.previous
    lex := string_token.lexeme
    contents := lex[1:len(lex)-1]

    idx, ok := vm.has_string(contents)
    if ok {
        emit_loadop(idx)
        return
    }

    
    str_bytes : []u8 = make([]u8, len(contents))
    for idx in 0..<len(contents) {
        str_bytes[idx] = contents[idx]
    }
    str_obj_val := vm.String {
        len = len(contents),
        data = str_bytes,
    }

    str_obj := vm.Obj {
        obj_value = cast(vm.ObjVal)str_obj_val
    }

    vm.track_obj(str_obj)

    value := cast(vm.Value)str_obj

    constant_idx := emit_constant(value)

    vm.intern_string(contents, constant_idx)
}


named_variable :: proc(name: Token, can_assign: bool) {
    arg : u8 = identifier_constant(name)
    if can_assign && match(.EQUAL) {
        expression()
        emit_bytes(cast(u8) vm.OpCode.OP_SET_GLOBAL, arg)
    } else {
        emit_bytes(cast(u8) vm.OpCode.OP_GET_GLOBAL, arg)
    }
}

variable :: proc(can_assign: bool) {
    named_variable(parser.previous, can_assign)
}


rules := [TokenType]ParseRule {
    .LEFT_PAREN = ParseRule { grouping, nil, .NONE },
    .RIGHT_PAREN = ParseRule { nil, nil, .NONE },
    .LEFT_BRACE = ParseRule { nil, nil, .NONE },
    .RIGHT_BRACE = ParseRule { nil, nil, .NONE },
    .COMMA = ParseRule { nil, nil, .NONE },
    .DOT = ParseRule { nil, nil, .NONE },
    .MINUS = ParseRule { unary, binary, .TERM },
    .PLUS = ParseRule { nil, binary, .TERM },
    .SEMICOLON = ParseRule { nil, nil, .NONE },
    .SLASH = ParseRule { nil, binary, .FACTOR },
    .STAR = ParseRule { nil, binary, .FACTOR },
    .BANG = ParseRule { unary, nil, .NONE },
    .BANG_EQUAL = ParseRule { nil, nil, .NONE },
    .EQUAL = ParseRule { nil, nil, .NONE },
    .EQUAL_EQUAL = ParseRule { nil, binary, .EQUALITY },
    .GREATER = ParseRule { nil, binary, .COMPARISON },
    .GREATER_EQUAL = ParseRule { nil, binary, .COMPARISON },
    .LESS = ParseRule { nil, binary, .COMPARISON },
    .LESS_EQUAL = ParseRule { nil, binary, .COMPARISON },
    .IDENTIFIER = ParseRule { variable, nil, .NONE },
    .STRING = ParseRule { compile_string, nil, .NONE },
    .NUMBER = ParseRule { number, nil, .NONE },
    .AND = ParseRule { nil, nil, .NONE },
    .CLASS = ParseRule { nil, nil, .NONE },
    .ELSE = ParseRule { nil, nil, .NONE },
    .FALSE = ParseRule { literal, nil, .NONE },
    .FOR = ParseRule { nil, nil, .NONE },
    .FUN = ParseRule { nil, nil, .NONE },
    .IF = ParseRule { nil, nil, .NONE },
    .NIL = ParseRule { literal, nil, .NONE },
    .OR = ParseRule { nil, nil, .NONE },
    .PRINT = ParseRule { nil, nil, .NONE },
    .RETURN = ParseRule { nil, nil, .NONE },
    .SUPER = ParseRule { nil, nil, .NONE },
    .THIS = ParseRule { nil, nil, .NONE },
    .TRUE = ParseRule { literal, nil, .NONE },
    .VAR = ParseRule { nil, nil, .NONE },
    .WHILE = ParseRule { nil, nil, .NONE },
    .IGNORE = ParseRule { nil, nil, .NONE },
    .ERROR = ParseRule { nil, nil, .NONE },
    .EOF = ParseRule { nil, nil, .NONE },
}

parse_precedence :: proc(precedence: Precedence) {
    advance()
    // We consider two tokens at a time (called previous and current)
    // Tokens       : t1 t2 t3 t4
    // After advance: ^  ^ 

    prefix_rule := get_rule(parser.previous.type).prefix
    if prefix_rule == nil {
        error("Expected expression.")
        return
    }

    can_assign := precedence <= Precedence.ASSIGNMENT
    prefix_rule(can_assign)
    // This rule may consume many tokens
    // Once it's done, the current token should point to the next, unprocessed token
    // If it's a number we still have:
    // Tokens       : t1 t2 t3 t4
    // After advance: ^  ^ 

    for (precedence <= get_rule(parser.current.type).precedence) {
        advance()
        // Tokens       : t1 t2 t3 t4
        // After advance:    ^  ^ 
        infix_rule := get_rule(parser.previous.type).infix
        infix_rule(can_assign)
    }

    if can_assign && match(.EQUAL) {
        error("Invalid assignment target.")
    }
}


define_variable :: proc(global: u8) {
    if (current.scope_depth > 0) {
        return
    }

    emit_bytes(cast(u8)vm.OpCode.OP_DEFINE_GLOBAL, global)
}


identifier_constant :: proc(name: Token) -> u8 {
    idx, ok := vm.has_string(name.lexeme)
    if ok {
        return idx
    }
    result := make_constant(cast(vm.Value) vm.string_to_obj(name.lexeme))
    return result
}


add_local :: proc(name: Token) {
    if (current.local_count == MAX_LOCALS) {
        error("Too many local variables in function.")
        return
    }

    local : Local = current.locals[current.local_count]
    current.local_count += 1
    local.name = name
    local.depth = current.scope_depth
}


identifiers_equal :: proc(a: Token, b: Token) -> bool {
    if (len(a.lexeme) != len(b.lexeme)) {
        return false
    }
    for i in 0..<len(a.lexeme) {
        if a.lexeme[i] != b.lexeme[i] {
            return false
        }
    }

    return true
}


declare_variable :: proc() {
    if (current.scope_depth == 0) {
        return;
    }
    name := parser.previous
    i := current.local_count - 1

    for i >= 0 {
        local := current.locals[i]
        if (local.depth != -1 && local.depth < current.scope_depth) {
            break
        }

        if (identifiers_equal(name, local.name)) {
            error("A variable with this name has already been defined in this scope")
        }
    }


    add_local(name)
}


parse_variable :: proc(error_message: string) -> (u8, string) {
    consume(.IDENTIFIER, error_message)
    ident := parser.previous.lexeme
    

    declare_variable()
    if (current.scope_depth > 0) {
        return 0, ident
    }


    result := make_constant(cast(vm.Value) vm.string_to_obj(ident))
    return result, ident
}

emit_constant :: proc(value: vm.Value) -> u8 {
    return make_constant(value)
}

emit_loadop :: proc(idx: u8) {
    emit_byte(cast(u8)vm.OpCode.OP_CONSTANT)
    emit_byte(idx)
}

make_constant :: proc(value: vm.Value) -> u8 {
    constant := vm.add_constant(current_chunk(), value, parser.previous.line)
    if constant > 255 {
        error("Too many constants in one chunk.")
        return 0
    }

    return cast(u8) constant
}



expression :: proc() {
    parse_precedence(.ASSIGNMENT)
}


check :: proc(expected: TokenType) -> bool {
    return parser.current.type == expected
}

match :: proc(expected: TokenType) -> bool {
    if !check(expected) {
        return false
    }
    advance()
    return true
}


print_statement :: proc() {
    expression()
    consume(.SEMICOLON, "Expected ';' after value.")
    emit_byte(cast(u8)vm.OpCode.OP_PRINT)
}


expression_statement :: proc() {
    expression()
    consume(.SEMICOLON, "Expected ';' after expression.")
    emit_byte(cast(u8)vm.OpCode.OP_POP)
}


block :: proc() {
    for (!check(.RIGHT_BRACE) && !check(.EOF)) {
        declaration()
    }

    consume(.RIGHT_BRACE, "Expected '}' after a block.")
}


begin_scope :: proc() {
    current.scope_depth += 1
}

end_scope :: proc() {
    current.scope_depth -= 1


    for (current.local_count > 0 && current.locals[current.local_count - 1].depth > current.scope_depth) {
        emit_byte(cast(u8)vm.OpCode.OP_POP)
        current.local_count -= 1
    }
}


statement :: proc() {
    if match(.PRINT) {
        print_statement()
    } else if match(.LEFT_BRACE) {
        begin_scope()
        block()
        end_scope()
    } else {
        expression_statement()
    }
}


synchronize :: proc() {
    parser.panic_mode = false 

    for parser.current.type != .EOF {
        if parser.previous.type == .SEMICOLON {
            return
        }

        #partial switch parser.current.type {
            case .CLASS, .FUN, .VAR, .FOR, .IF, .WHILE, .PRINT, .RETURN:
                return
            case:
                // Do nothing.
        }

        advance()
    }
}


var_declaration :: proc() {
    global, ident := parse_variable("Expected variable name.")

    if match(.EQUAL) {
        expression()
    } else {
        emit_byte(cast(u8)vm.OpCode.OP_NIL)
    }

    consume(.SEMICOLON, "Expected ';' after variable declaration.")

    define_variable(global)
    vm.intern_string(ident, global)
}

declaration :: proc() {
    if match(.VAR) {
        var_declaration()
    } else {
        statement()
    }

    if (parser.panic_mode) {
        synchronize()
    }
}


compile :: proc(source: string, chunk: ^vm.Chunk) -> bool {
    init_tokenizer(source)
    compiler := Compiler {}
    init_compiler(&compiler)
    compiling_chunk = chunk
    parser.had_error = false
    parser.panic_mode = false

    advance()

    for !match(.EOF) {
        declaration()
    }

    consume(.EOF, "Expected end of file.")

    end_compiler()

    return !parser.had_error 
}
