package main

import "core:fmt"
import "core:os"
import vm "vm"
import comp "compiler"


interpret :: proc(source: string) -> vm.InterpretResult {
    chunk := vm.make_chunk()
    defer vm.delete_chunk(chunk)

    // Compile
    if (!comp.compile(source, chunk)) {
        return .COMPILE_ERROR
    }

    // Execute
    result := vm.interpret(chunk)

    return result 
}

repl :: proc() {
    vm.init_vm()
    defer vm.free_vm()
    buffer := make([]u8, 1024)

    for {
        fmt.print("> ")
        // READ
        n_bytes_read, errno := os.read(os.stdin, buffer)
        // fmt.println("Read ", buffer[:n_bytes_read])

        if n_bytes_read == 0 || (n_bytes_read == 1 && buffer[0] == 10) {
            break
        }

        // EVAL
        input := cast(string) buffer[:n_bytes_read]
        result:= interpret(input)
        if result != .OK {
            fmt.println("Failed to interpret")
            continue
        }
        // fmt.println("Got:", input)

        // PRINT
        // TODO

        // LOOP
    }
    fmt.println("")
}



main :: proc() {
    args := os.args

    switch len(args) {
        case 1:
            // No input file given, start repl
            repl()
        case 2:
            // Assume we were given a file, read it, compile, and execute
            bytes, success := os.read_entire_file_from_filename(args[1])
            file_contents := cast(string) bytes
            result := interpret(file_contents)
            fmt.println(file_contents)
        case:
            fmt.println("Usage:")
            fmt.println("cic            <--- Start REPL")
            fmt.println("cic [filename] <--- Compile and run file")
    }
}




