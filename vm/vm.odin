package vm 

import "core:mem"
import "core:fmt"
import "core:os"


_DEBUG :: false 
DEBUG       :: true 
DEBUG_TRACE :: _DEBUG

VM :: struct {
    chunk: ^Chunk,
    ip: ^u8, // Pointer into chunk's code
    stack: ^Stack,
    objects: [dynamic]Obj,
    strings: map[string]u8,
    globals: map[string]Value
}

vm : VM


init_vm :: proc() {
    vm = VM {
        chunk = nil
        stack = make_stack(STACK_CAP)
        objects = make([dynamic]Obj, 0)
        strings = make(map[string]u8)
        globals = make(map[string]Value)
    }
}

track_obj :: proc(obj: Obj) {
    append(&vm.objects, obj)
}


free_obj :: proc(obj: Obj) {
    switch obj_value in obj.obj_value {
        case String:
            delete(obj_value.data)
        case:
            break;
    }
}

free_vm :: proc() {
    when DEBUG {
        fmt.println("Freeing VM")
    }
    for obj in vm.objects {
        free_obj(obj)
    }
    delete(vm.objects)
    delete(vm.strings)
    delete(vm.globals)
}


has_string :: proc(str: string) -> (u8, bool) {
    idx, ok := vm.strings[str]
    return idx, ok
}

intern_string :: proc(str: string, obj_idx: u8) {
    vm.strings[str] = obj_idx 
}


InterpretResult :: enum {
    OK,
    COMPILE_ERROR,
    RUNTIME_ERROR
}


interpret :: proc(chunk: ^Chunk) -> InterpretResult {
    vm.chunk = chunk
    vm.ip = &chunk.code[0] 
    
    result := run() 

    return result
}


read_byte :: #force_inline proc() -> u8 {
    value := vm.ip^
    vm.ip = mem.ptr_offset(vm.ip, 1)

    return value
}

read_constant :: #force_inline proc() -> Value {
    address := read_byte()
    value := vm.chunk.constants[address]

    return value
}

read_string :: #force_inline proc() -> string {
    value := read_constant()
    obj, ok_obj := value.(Obj)
    if !ok_obj {
        fmt.println("Expected string constant")
        os.exit(1)
    }

    str, ok := obj.obj_value.(String)
    if !ok {
        fmt.println("Expected string constant")
        os.exit(1)
    }

    str_val := cast(string)str.data
    return str_val
}


apply :: proc(op: proc(a: Value, b: Value) -> Value) {
    b := pop(vm.stack)
    a := pop(vm.stack)

    result := op(a, b)

    push(vm.stack, result)
}

run_add :: proc() -> bool {
    b := pop(vm.stack)
    a := pop(vm.stack)

    #partial switch val_a in a {
        case f64:
            val_b, ok := b.(f64)
            if ok {
                result := val_a + val_b 
                push(vm.stack, result)
                return true 
            }  else {
                // b is not f64
                return false
            }
        case Obj:
            switch obj_val in val_a.obj_value {
                case String:
                    obj_b, ok_b := b.(Obj)
                    if !ok_b {
                        return false
                    }

                    str_b, ok := obj_b.obj_value.(String)
                    if !ok {
                        return false
                    }

                    conc_len := obj_val.len + str_b.len
                    conc_data := make([]u8, conc_len)

                    for i in 0..<obj_val.len {
                        conc_data[i] = obj_val.data[i]
                    }
                    for i in 0..<str_b.len {
                        conc_data[i + obj_val.len] = str_b.data[i]
                    }

                    conc_string : String = {
                        len = conc_len,
                        data = conc_data
                    }
                    
                    conc_obj := Obj {
                        obj_value = conc_string
                    }

                    value := cast(Value)cast(Obj)conc_obj

                    push(vm.stack, value)
                    
                    // TODO: Who frees this string?
                    return true

                case:
                    return false
            }
       case: 
       return false 
    }
}

run_minus :: proc() -> bool {
    b := pop(vm.stack)
    a := pop(vm.stack)

    #partial switch val_a in a {
        case f64:
            val_b, ok := b.(f64)
            if ok {
                result := val_a - val_b 
                push(vm.stack, result)
                return true 
            } else {
                // b is not f64
                return false
            }
       case: 
       return false 
    }
}

run_mul :: proc() -> bool {
    b := pop(vm.stack)
    a := pop(vm.stack)

    #partial switch val_a in a {
        case f64:
            val_b, ok := b.(f64)
            if ok {
                result := val_a * val_b 
                push(vm.stack, result)
                return true 
            } else {
                // b is not f64
                return false
            }
       case: 
       return false 
    }
}

run_div :: proc() -> bool {
    b := pop(vm.stack)
    a := pop(vm.stack)

    #partial switch val_a in a {
        case f64:
            val_b, ok := b.(f64)
            if ok {
                result := val_a / val_b 
                push(vm.stack, result)
                return true 
            } else {
                // b is not f64
                return false
            }
       case: 
       return false 
    }
}

run_greater :: proc() -> bool {
    b := pop(vm.stack)
    a := pop(vm.stack)

    #partial switch val_a in a {
        case f64:
            val_b, ok := b.(f64)
            if ok {
                result := val_a > val_b 
                push(vm.stack, result)
                return true 
            } else {
                // b is not f64
                return false
            }
       case: 
       return false 
    }
}


run_less :: proc() -> bool {
    b := pop(vm.stack)
    a := pop(vm.stack)

    #partial switch val_a in a {
        case f64:
            val_b, ok := b.(f64)
            if ok {
                result := val_a < val_b 
                push(vm.stack, result)
                return true 
            } else {
                // b is not f64
                return false
            }
       case: 
       return false 
    }
}


run_equal :: proc() -> bool {
    b := pop(vm.stack)
    a := pop(vm.stack)
    #partial switch val_a in a {
        case Obj:
            obj_b, ok := b.(Obj)
            if ok {
                str_a, ok_a := val_a.obj_value.(String)
                str_b, ok_b := obj_b.obj_value.(String)
                if ok_a && ok_b {
                    push(vm.stack, String_equal(str_a, str_b))
                    return true
                } else {
                    push(vm.stack, false)
                    return true 
                }
            } else {
                push(vm.stack, false)
                return true 
            }
        case Nil:
            _, ok := b.(Nil)
            push(vm.stack, ok)
            return true
        case bool:
            val_b, ok := b.(bool)
            if ok {
                push(vm.stack, val_a == val_b)
                return true
            } else {
                push(vm.stack, false)
                return true 
            }
        case f64:
            val_b, ok := b.(f64)
            if ok {
                push(vm.stack, val_a == val_b)
                return true
            } else {
                push(vm.stack, false)
                return true 
            }
        }

    return false
}


is_falsy :: proc(value: Value) -> Value {
    #partial switch v in value {
        case f64:
            return v == 0.0
        case bool:
            return !v
        case Nil:
            return true
        case:
            return false
    }

}


run :: proc() -> InterpretResult { // TODO: Also return an error message
    for {
        when DEBUG_TRACE {
            // Print the contents of the stack
            fmt.println("---STACK---")
            fmt.print("[")
            for value, i in &vm.stack.values {
                if &value == vm.stack.top {
                    break
                }
                if i > 0 {
                    fmt.print(", ")
                }

                fmt.print(value)
            }
            fmt.println("]")
            fmt.println("--/STACK---")
        }



        instruction := cast(OpCode) read_byte()
        switch instruction {
            case .OP_CONSTANT:
                value := read_constant()
                push(vm.stack, value)
                when DEBUG_TRACE {
                    fmt.println("Value:", value)
                }
            case .OP_NIL:
                push(vm.stack, make_nil())
            case .OP_FALSE:
                push(vm.stack, make_bool(false))
            case .OP_EQUAL:
                ok := run_equal()
                if !ok {
                    return .RUNTIME_ERROR
                }
            case .OP_GREATER:
                ok := run_greater()
                if !ok {
                    return .RUNTIME_ERROR
                }
            case .OP_LESS:
                ok := run_less()
                if !ok {
                    return .RUNTIME_ERROR
                }
            case .OP_TRUE:
                push(vm.stack, make_bool(true))
            case .OP_ADD: 
                ok := run_add() 
                if !ok {
                    return .RUNTIME_ERROR
                }
            case .OP_SUB: 
                ok := run_minus() 
                if !ok {
                    return .RUNTIME_ERROR
                }
            case .OP_MUL: 
                ok := run_mul() 
                if !ok {
                    return .RUNTIME_ERROR
                }
            case .OP_DIV: 
                ok := run_div() 
                if !ok {
                    return .RUNTIME_ERROR
                }
            case .OP_NEGATE:
                value := pop(vm.stack) 
                f_val, ok := value.(f64)
                if ok {
                    push(vm.stack, -f_val)
                } else {
                    return .RUNTIME_ERROR
                }
            case .OP_NOT:
                value := pop(vm.stack)
                new_value := is_falsy(value)
                push(vm.stack, new_value)
            case .OP_PRINT:
                print_value(pop(vm.stack))
                fmt.println()
            case .OP_POP:
                pop(vm.stack)
            case .OP_DEFINE_GLOBAL:
                name : string = read_string()
                vm.globals[name] = peek(vm.stack)
                pop(vm.stack)
            case .OP_GET_GLOBAL:
                name := read_string()
                value, ok := vm.globals[name]
                if !ok {
                    return .RUNTIME_ERROR
                }
                push(vm.stack, value)
            case .OP_SET_GLOBAL:
                name := read_string()
                _, ok := vm.globals[name]
                if !ok {
                    return .RUNTIME_ERROR
                }
                vm.globals[name] = peek(vm.stack)
            case .OP_RETURN:
                return .OK
        }
    }

    return .RUNTIME_ERROR
}
