package vm 

import "core:fmt"

Nil :: struct {}

String :: struct {
    len: int,
    data: []u8,
}

String_equal :: proc(str1: String, str2: String) -> bool {
    if str1.len != str2.len {
        return false
    }
    for i in 0..<str1.len {
        if str1.data[i] != str2.data[i] {
            return false
        }
    }
    return true
}

ObjVal :: union {
    String,
}

Obj :: struct {
    obj_value: ObjVal,
}


string_to_obj :: proc(str: string) -> Obj {
    
    str_bytes : []u8 = make([]u8, len(str))
    for idx in 0..<len(str) {
        str_bytes[idx] = str[idx]
    }
    str_obj_val := String {
        len = len(str),
        data = str_bytes,
    }

    str_obj := Obj {
        obj_value = cast(ObjVal)str_obj_val
    }

    return str_obj
}

// Value :: f64
Value :: union #no_nil {
    Nil,
    bool,
    f64,
    Obj,
}

print_value :: proc(value: Value) {
    switch val in value {
        case Nil:
            fmt.printf("nil")
        case bool:
            fmt.printf("%v", val)
        case f64:
            fmt.printf("%v", val)
        case Obj:
            str_val, ok := val.obj_value.(String)
            if !ok {
                fmt.println("UNREACHABLE")
            }

            str := cast(string)str_val.data 

            fmt.printf("\"%v\"", str)
    }
}

make_nil :: #force_inline proc() -> Value {
    return Nil {}
}

make_bool :: #force_inline proc(b: bool) -> Value {
    return b
}

make_f64 :: #force_inline proc(v: f64) -> Value {
    return v
}

ValueArray :: [dynamic]Value


to_string :: proc(value: Value) -> string {
    switch val in value {
        case Nil:
            return "nil"
        case bool:
            return fmt.aprintf("%v", val)
        case f64:
            return fmt.aprintf("%v", val)
        case Obj:
            str_val, ok := val.obj_value.(String)
            if !ok {
                fmt.println("to_string: UNREACHABLE")
            }

            str := cast(string)str_val.data 

            return fmt.aprintf("\"%v\"", str)
    }

    return "to_string: UNREACHABLE"
}
