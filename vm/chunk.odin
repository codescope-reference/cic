package vm 

import "core:fmt"
import "core:strings"
import "core:mem"


Chunk :: struct {
    code : [dynamic]u8,
    line_numbers: map[int]int,
    constants: ValueArray,
}

add_constant :: proc(chunk: ^Chunk, value: Value, line_number: int) -> int {
    append(&chunk.constants, value)
    constant_idx := len(chunk.constants) - 1

    add_op(chunk, .OP_CONSTANT, line_number)
    append(&chunk.code, cast(u8)constant_idx)

    return constant_idx
}

add_op :: proc(chunk: ^Chunk, op: OpCode, line_number: int) {
    append(&chunk.code, cast(u8)op)

    chunk.line_numbers[len(chunk.code) - 1] = line_number
}

write_chunk :: proc(chunk: ^Chunk, byte: u8, line_number: int) {
    append(&chunk.code, byte)
    chunk.line_numbers[len(chunk.code) - 1] = line_number
}


make_chunk :: proc(code_cap := 0, constants_cap := 0) -> ^Chunk {
    chunk := new(Chunk)
    code := make([dynamic]u8, 0, code_cap)
    constants := make(ValueArray, 0, constants_cap)
    line_nums := make(map[int]int)

    chunk.code = code
    chunk.constants = constants
    chunk.line_numbers = line_nums

    return chunk
}

delete_chunk :: proc(chunk: ^Chunk) {
    delete(chunk.code)
    delete(chunk.constants)
    delete(chunk.line_numbers)
    free(chunk)
}

get_value_type_str :: proc(value: Value) -> string {
    switch val in value {
        case Nil:
            return "Nil"
        case bool:
            return "boolean"
        case f64:
            return "number"
        case Obj:
            return "Obj"
    }
    return "unknown"
}

disassemble :: proc(chunk: ^Chunk) -> string {
    output : string = "Instructions:\n"
    temp : string = ""

    index := 0
    inst_counter := 0
    for index < len(chunk.code) {
        // Write OpCode
        byte_inst := chunk.code[index]
        op_code_str := OpCode(byte_inst)
        temp = strings.concatenate({temp, fmt.tprintf("  %04v %v", inst_counter, op_code_str)})
        line_number, err := chunk.line_numbers[index]
        inst_counter += 1

        // Handle OpCodes that have operands
        if op_code_str == .OP_CONSTANT {
            // Read the address (currently a single byte)
            // Print address next to OpCode
            // Advance pointer 2 bytes 

            address := chunk.code[index + 1]
            temp = strings.concatenate({temp, fmt.tprintf(" %v", address)})
            index += 1
        } 

        if op_code_str == .OP_DEFINE_GLOBAL || op_code_str == .OP_GET_GLOBAL || op_code_str == .OP_SET_GLOBAL {
            // Read the address (currently a single byte)
            address := chunk.code[index + 1]
            // Lookup constant value
            constant_str := to_string(chunk.constants[address])
            temp = strings.concatenate({temp, fmt.tprintf(" %v", constant_str)})
            delete(constant_str)
            // Advance index by one extra to skip the constant address
            index += 1
        }

        // Padding
        temp = strings.left_justify(temp, 30, " ")

        temp = strings.concatenate({temp, fmt.tprintf(" | Line: %04v", line_number)})
        temp = strings.concatenate({temp, "\n"})
        index += 1

        output = strings.concatenate({output, temp})
        temp = ""
    }


    output = strings.concatenate({output, "Constants:\n"})
    for constant, index in chunk.constants {
        const_obj, is_obj := constant.(Obj)
        if is_obj {
            output = strings.concatenate({output, fmt.tprintf("  %04v %v %v\n", index, const_obj.obj_value, get_value_type_str(constant))})
        } else {
            output = strings.concatenate({output, fmt.tprintf("  %04v %v %v\n", index, constant, get_value_type_str(constant))})
        }
    }

    output = strings.concatenate({output, "\n"})


    return output
}
