package vm 

import "core:fmt"
import "core:mem"
import "core:os"

Allocator       :: mem.Allocator
Allocator_Mode  :: mem.Allocator_Mode
Allocator_Error :: mem.Allocator_Error


my_allocator :: proc(my_allocator_data: ^MyAllocatorData) -> Allocator {
    return Allocator {
        procedure = my_allocator_proc,
        data      = my_allocator_data,
    }
}

MyAllocatorData :: struct {
    // arena : []byte,
    allocator : Allocator,
}

my_allocator_data_init :: proc(d: ^MyAllocatorData) {
    d.allocator = context.allocator
    // d.backing_data = backing
}

my_allocator_proc :: proc(allocator_data: rawptr, mode: Allocator_Mode,
                          size, alignment: int,
                          old_memory: rawptr, old_size: int, 
                          location := #caller_location) -> ([]byte, Allocator_Error) {
    allocator := (cast(^MyAllocatorData)allocator_data).allocator

    #partial switch mode {
        case .Alloc:
            // fmt.println("ALLOC")
            // return allocator.procedure(allocator_data, mode, size, alignment, old_memory, old_size, location)
            bytes, error := mem.alloc_bytes(size, alignment, allocator, location)
            // fmt.println("ALLOCATING")

            if error != nil {
                fmt.println("Failed to allocate. Exiting program")
                os.exit(1)
            }

            return bytes, nil
        case .Free:
            // fmt.println("FREE")
            return nil, mem.free(cast(rawptr)old_memory, allocator, location)
        case .Resize:
            bytes, error := allocator.procedure(allocator_data, mode, size, alignment, old_memory, old_size, location)
            // fmt.println("RESIZE")

            if error != nil {
                fmt.println("Failed to resize. Exiting program")
                os.exit(1)
            }

            return bytes, nil
        case .Alloc_Non_Zeroed:
            bytes, error := allocator.procedure(allocator_data, mode, size, alignment, old_memory, old_size, location)
            // fmt.println("ALLOC_NON_ZEROED")

            if error != nil {
                fmt.println("Failed call to Alloc_Non_Zeroed. Exiting program")
                os.exit(1)
            }

            return bytes, nil
        }

    fmt.println("Mode", mode, "not supported. Exiting.")
    os.exit(1)
    // return nil, .Mode_Not_Implemented



    // fmt.println("Called with size", size);
    // fmt.println("Called with old_size", old_size);
    // fmt.println("Called with mode", mode);

    // data := cast(^MyAllocatorData)allocator_data

    // // byte_slice := mem.byte_slice(data.backing_data, size)
    // byte_slice := data.backing_data[0:size]

    // return byte_slice, nil
}




main :: proc () {
    // stack : ^Stack = make_stack(8)
    // defer delete_stack(stack)
    // push(stack, 0.1)
    // fmt.println(stack.top)
    // value := pop(stack)
    // fmt.println("Popped:", value)
    // fmt.println(stack.values)
    // my_data := MyAllocatorData { }
    // my_allocator_data_init(&my_data);
    // allocator := my_allocator(&my_data)
    // context.allocator = allocator

    init_vm()
    defer free_vm()

    // // Set up test bytecode
    chunk := make_chunk()
    defer delete_chunk(chunk)
    // 3 * 2 + 1 - 15
    add_constant(chunk, 3, 1)
    add_constant(chunk, 2, 2)
    add_op(chunk, .OP_MUL, 3)
    add_constant(chunk, 1, 2)
    add_op(chunk, .OP_ADD, 3)
    add_constant(chunk, 15, 2)
    add_op(chunk, .OP_SUB, 0)
    add_op(chunk, .OP_RETURN, 5)

    // // Print bytecode for reference
    assembly := disassemble(chunk)
    fmt.println("Disassembled:")
    fmt.println(assembly)

    interpret(chunk)
}
